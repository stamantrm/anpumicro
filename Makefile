NAME = paper

tex = $(wildcard *.tex) $(wildcard *.sty) $(wildcard *.cls) $(wildcard Fig/*.pdf) $(wildcard *.bib) $(wildcard Table/*.tex) $(wildcard Listing/*.tex)

all: $(NAME).pdf

$(NAME).pdf: $(NAME).bbl $(NAME).tex $(tex) $(figs)
	pdflatex $(NAME).tex; pdflatex $(NAME).tex

outline: outline.tex
	pdflatex outline.tex

chmod:
	chmod -R 774 *
	chgrp -R cartel *

$(NAME).bbl: $(NAME).bib
	pdflatex $(NAME).tex; bibtex $(NAME)

view: $(NAME).pdf
	gv $(NAME).pdf

open:
	open $(NAME).pdf

clean:
	rm -f *~ *.bbl *.lof *.out *.lot *.log *.toc *.blg *.dvi *.aux *.ps *.synctex.gz
	rm -f $(NAME).pdf
