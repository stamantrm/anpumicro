\section{Preliminary Bit-Width Analysis}
\label{sec:eval}

This section describes our evaluation setup and a preliminary bit-width
evaluation for the APE design described in the previous section.

\paragraph{Evaluation setup.}
We use a combination of software simulation and hardware design tools
to estimate the accuracy and potential energy efficiency of an analog
NPU implementation. 
An ANPU software simulator allows for quick modeling. 
Additionally, transistor-level analog circuits, implemented and
simulated in the Cadence
Analog Design Environment using Predictive Technology Models (PTMs) at
45nm~\cite{PTM}, give detailed power, accuracy, and latency
information.
We evaluate the same benchmarks as Esmaeilzadeh et al., allowing
for easy comparison to a digital design~\cite{NPU_MICRO}.
These benchmarks cover a range of application domains, including signal and image processing,
3D gaming, machine learning, compression and robotics.   



\if 0

\subsubsection{Methodology}

We use a combination of software simulation and hardware design tools to estimate the accuracy and energy efficiency
of a range of ANPU designs along with their effect on application-level accuracy, speedup, and energy consumption. 
Our simulation environment consists of four pieces: a software simulator that models an ANPU, 
circuit-level simulations in the Cadence Analog Design Environment, a cycle-accurate x86-64 simulator to model the non-accelerated 
portion of each application and capture application-level effects,
and power modeling tools that are used to get energy estimates for the CPU and some elements of the ANPU, 
such as digital storage and wiring.

The ANPU simulator allows modeling of a variety of APE configurations, taking into account analog circuit behavior found through
Cadence analog circuit implementations. One advantage of software simulation is the ability to more thoroughly and quickly 
investigate the design space with a varying degree of estimation accuracy, without necessarily requiring a new circuit
design at each point in the space. After learning the best place to focus our circuit design efforts,  
transistor-level analog circuits will be implemented in the Cadence
Analog Design Environment using Predictive Technology Models (PTMs) at 45nm~\cite{PTM}. Circuit simulations give accuracy,
power, and latency information for any particular design point. These
estimates are characterized and plugged into the ANPU simulator, which
interfaces with the CPU simulator. We use the same benchmarks, cycle-accurate simulation infrastructure, 
and power modeling framework as Esmaeilzadeh et al., described in the previous section. 
This choice allows for a solid comparison between a digital implementation and an analog one. 


%\subsubsection{Preliminary Results}
\fi

\paragraph{Opportunity for energy savings.}

\begin{table}
  \centering
  \caption{Power breakdown for an 8-wide APE with 5-bit inputs and 4-bit weights}
  \includegraphics[width=0.25\textwidth]{./figs/Anpu/powerTable.pdf}
  \label{table:APEpower}
\end{table}

To determine estimates for comparing analog and digital computation efficiency, 
we have implemented a single APE in Cadence at 45 nm. % as shown in Figure~\ref{fig:ape}. 
This 8-wide APE computes on 5-bit inputs and their corresponding 4-bit weights. 
%We chose eight inputs because this number is sufficient for a majority of 
%the applications studied~\cite{NPU_WEED}. 
Table~\ref{table:APEpower}, shows the power breakdown for the analog components. 
A large portion of the power consumption can be attributed to the dynamic power of the ADC due to the numerous switching comparators.
%As described later in this section, future designs will attempt to minimize analog-to-digital conversions. 
The latch time required for the circuit to produce the most accurate results is between 800 and 1000 ps, corresponding to a clock rate of
1 to 1.25 GHz. The average mean-squared error is 0.015625. For the implemented 6-bit ADC, this corresponds 
to one quantization step, e.g. an output value of 22 rather than the expected output of 21. 
At this design point, the error can mostly be attributed to final signal
perturbation due to the ADC switching comparators. Without the affect
of the ADC, accuracy is higher.
%The accuracy of internal circuit signals is higher than what is suggested by the reported MSE.


\begin{figure}
\centering
    \subfloat[Analog/digital energy comparison for various input bit-width design points]{
        \includegraphics[width=0.35\textwidth]{./figs/Anpu/energyEstimate.pdf}\label{fig:energyop}}
        
    \subfloat[Analog energy savings for various input bit-width design points]{
        \includegraphics[width=0.35\textwidth]{./figs/Anpu/energySavingsEstimate.pdf}\label{fig:energysavings}}
        
    \caption{Potential energy savings for the multiply-add operation}
    \label{fig:roughenergy}
\end{figure}


For input mapping using a current-steering DAC, power consumption increases
exponentially with the number of bits converted.  To estimate a
break-even point with a digital implementation, we've extrapolated
power measurements from the 5-bit APE implemented in
Cadence. Figure~\ref{fig:roughenergy} shows energy estimates for
a single multiply-add operation, comparing various analog design
points with a 16-bit digital floating point design~\cite{Galal:2011}.
For this estimate, we assume that digital-to-analog conversions are
required for each computation.  
Figure~\ref{fig:energysavings} shows that with 5-bit input
conversions, 100$\times$ energy savings is possible. 
Potential energy savings for an analog
implementation with 8-bit inputs
is 10$\times$.  The break-even point for energy
consumption is estimated at 11 bits with respect to a 16-bit
floating-point unit running at 1.25 GHz and 12 bits with respect to
one running at 2.08 GHz. In addition to the multiply-add operation, an
analog design may be more energy efficient in the sigmoid computation,
bus and scheduling requirements, and storage accesses.  Also, this
graph mostly displays the increase in power due to data conversion.
For a physical configuration in which data conversions are ammortized over
a larger amount of computation, for example, in a configuration that
contains multiple rows of APEs, where the outputs of one row are fed
directly to the inputs of the following row, substantially more energy savings is attainable. 
%Removing conversions between the analog and digital domain allow for
%substantially more energy savings. 
 %Figure~\ref{fig:energysavings} shows that even with 5-bit conversions, 100$\times$ energy savings is
%possible. 


\begin{figure}
  \centering
  \includegraphics[width=0.45\textwidth]{./figs/Anpu/mseSine.pdf}
  \caption{Accuracy upper bound for various APE design points}
  \label{fig:msesine}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.45\textwidth]{./figs/Anpu/mseSobel.pdf}
  \caption{Accuracy upper bound for various APE design points}
  \label{fig:msesobel}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.45\textwidth]{./figs/Anpu/mseInversek2j.pdf}
  \caption{Accuracy upper bound for various APE design points}
  \label{fig:mseinversek2j}
\end{figure}

\paragraph{Potential accuracy.}

One possible cause of ANPU imprecision is computation using a limited
number of bits of the digital inputs and weights.
To get an accuracy upper bound for an ANPU using a limited number of bits, 
we've simulated the compiler-chosen neural network that
mimics the approximation-tolerant code region for each benchmark in the ANPU simulator. 
We assume a physical configuration of a single row, with the number of operations per APE as large as necessary for that
application. For example, for {\em Sobel}, which has a 
9$\rightarrow$8$\rightarrow$1 neural network topology (9 inputs,
8-neuron hidden layer, and 1 output), we assume an ANPU with one row of 8 APEs, each APE capable of accepting 9 inputs and doing
9 multiply-add operations. The simulator accounts for quantization errors due to the limited-precision, 
analog-to-digital conversion between layers. 

Figures~\ref{fig:msesine},~\ref{fig:msesobel},~and~\ref{fig:mseinversek2j}
show the accuracy upper bound for their respective neural network computation, reported as mean-squared error (MSE), 
for various design points of input bit-width and weight bit-width. 
ADC output bit-width is assumed to be the same as the input
bit-width. For brevity, we give results for three of the applications
studied; however, results for the other benchmarks simulated show similar
trends. 
For the {\em Sine} application, Figure~\ref{fig:msesine} shows that 5-bit inputs and 7-bit weights 
can achieve accuracy comparable to a fully precise, digital implementation. 
Figure~\ref{fig:msesobel} shows that for {\em Sobel}, 10-bit weights keep error rates under 0.5\%. 
For {\em Inversek2j} in Figure ~\ref{fig:mseinversek2j}, 
increasing the number of input bits above 7 results in little
improvement in terms of accuracy. 
For the applications studied, increasing the input bit-width above 8 
results in little improvement in potential accuracy. 
As shown previously in Figure~\ref{fig:energysavings}, limiting the number of input bits to 8
may result in an order of magnitude energy savings over a digital
implementation, even with an expensive digital-to-analog conversion at each
computation step.

The initial set of results is promising, showing that an analog
implementation could potentially achieve accuracy comparable to a
fully-precise digital version, while still maintaining advantages in
energy-efficiency. We are currently working to determine the actual circuit
accuracy for a range of bit widths, since although in theory the
achievable accuracy increases up to 8 bits, actual circuit accuracy
becomes less likely to hit the potential target as ranges increase
and input mapping degrades with an increased number of bits. 



%\subsubsection{Conclusion} % conclusion for the evaluation section 
%\paragraph{Conclusion.} 

% The initial set of results is promising, showing that an analog
% implementation could potentially achieve accuracy comparable to a
% fully-precise digital version, while still maintaining advantages in
% energy-efficiency. Future work, described in Chapter~\ref{ch:plan},
% will evaluate additional benchmarks, chose design points to implement
% at the circuit level, gather power and accuracy measurements that will
% be fed back into the software simulator, eventually outlining results
% at the application level.



