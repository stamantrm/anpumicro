\section{Analog NPU Design Tradeoffs}
\label{sec:tradeoffs}

%\subsection{Opportunities}
% low power
%\subsection{Challenges}
% precision, flexibility

\begin{figure}
	\centering
    \includegraphics[width=1.0\linewidth]{./figs/Anpu/NpuDesignSpace.pdf}
    \caption{Design space of NPU implementations}
    \label{fig:npu-design-space}
\end{figure}

Figure~\ref{fig:npu-design-space} shows the design space of 
possible NPU implementations, which bears tradeoffs
in flexibility, accuracy, and energy efficiency.
Flexibility is desirable in order to enable computation
of various network topologies, thus increasing
the potential accuracy in which the compiler-generated, transformed
code segment mimics programmer-intended code for any particular program. 
At the analog point in the space, computation accuracy
is compromised, though gains in energy efficiency may 
allow more (or more complex) computation to be done, resulting in 
overall accuracy improvements over a digital design operating within
the same power budget. 
The work described in Section~\ref{sec:back} focuses on a digital ASIC design.  This work
investigates the analog ASIC point in the space, with the goal of
providing a framework for the design of an analog
neural processing unit (ANPU).  

%looking to understand
%the potential benefits of an analog approach in terms of power and latency, as
%well as challenges in flexibility and accuracy. 

%This section starts with an overview of the tradeoffs and challenges in an
%analog approach, followed by the design of an analog processing engine (APE)
%and a configuration of APEs that constitutes an ANPU. 

%followed by an initial ANPU design, evaluation methodology, and preliminary results. 
%Section\ref{sec:future} describes the research direction for the remaining work. 

%Although there has been a significant amount 
%of work done in mapping neural networks to analog circuit, it has not been done
%in the context of accelerating general-purpose, high-performance program code (ADD CITATIONS).
%Additionally, new technologies, such as resistive memories, may present new opportunities 
%for such a design.  

%This section describes
%deisgn space: it's possible that using low-power analog, we can get accuracy as good as the digital, 
%using less power, and computing in a shorter amount of time (less energy). 

%we may also be able to simulate larger networks with the same amount of power.
%the question is - how to arrange these networks. can we get reasonable power? with what accuracy?
%the goal is to identify the opportunities and challenges with analog
%design. 

\subsection{Considerations for an Analog Design}

The job of the ANPU is to efficiently
compute the outputs of a multilayered neural network given a set of
inputs, weights, and a network topology. Similar to the digital unit described in
the previous section, we define a basic computation unit that is
capable of computing the output of a sigmoid-activated neuron, where a sigmoid function
is applied to the dot product of a set of inputs and their associated
weights. We call this basic computation block an analog processing
engine, or APE. APEs can be combined, in various physical configurations,
along with control and storage, to form an ANPU.
 
The high-level goal of the ANPU is to provide sufficiently accurate
results through accelerated execution of approximation-tolerant code for
the largest number of applications possible. 
An attempt to increase application coverage, however, often degrades achievable
accuracy due to ideal operating ranges within an analog design. We define three design
components that provide a framework for ANPU design, outline the
tradeoffs and challenges associated with these that are unique to an
analog design, and explore their relationship
to overall utility in terms of application coverage (flexibility) and
achievable accuracy. 

%The designs of both the APE and the ANPU are subject to the challenges and tradeoffs outlined below, 
%which are unique to an analog approach. 

%General things
%Discuss space - totally reconfigurable to ASICs. we want something that can cover as much as possible. 
%More energy-efficient at the ASIC side. More application coverage at
%the reconfigurable side.

%A flexible topology increases application coverage, though decreases
%accuracy. Same for bit-width. 

\paragraph{Value representation.}

At the top level, digital data sent to the ANPU must be converted to
the analog domain for computation. Digital values could be represented
in a number of different ways, e.g. as analog currents or voltages on
one or more wires. 
The way in which information is represented in the analog domain affects the power, speed,
and accuracy of the computation blocks. For example, addition may be
performed extremely fast using currents, where as 
a multiply operation may be more efficiently executed using voltages. 
In addition to operating most-efficiently on a particular type of representation,
analog compute components often only operate efficiently or accurately on a particular range of values. 
Conversion between representations and signal scaling are unique challenges in the analog domain.
Finally, analog values that represent the
final network outputs must be converted to digital ones to be passed
back to the CPU. 
Conversions between the analog and digital domains
consume significant amounts of power and introduce inaccuracy due to
quantization, limiting the potential benefit of analog
computation. Ideally, the number of data conversions between these domains is limited.  

%and analog values that represent the
%final network outputs must be converted to digital ones to be passed
%back to the CPU. Intermediate values, however, and weight values can be stored in the
%either domain. 
%One challenge is to accurately store analog values.

\paragraph{Physical configuration.}
Another major design component is the 
physical arrangement of APEs, their
connectivity, and the associated control and storage. 
Unlike a digital design that takes advantage of time-multiplexed
computation to provide flexibility for implementing arbitrarily sized
networks (therefore increasing the scope of applications that
can utilize it), analog designs often gain efficiency through increasing the amount of
simultaneous computation.
For example, current values can be efficiently added together, though
they must be available for computation at the same time. 
This fact would support the inclusion of many APEs, enough to
simultaneously compute the largest networks that may be selected in
the compilation phase with no intermediate storage or time multiplexing. 
Increases in computation, however, 
increase the range of possible values at various circuit points. 
Since an analog computation unit produces more accurate
results when it is optimized for a small range of inputs, accuracy is
compromised for gains in efficiency and flexibility.  
One challenge is to determine a
configuration of APEs that capitalizes on opportunities for efficiency
and flexibility, while maintaining high accuracy across the applications covered. 



% For example, an APE is able to very
% efficiently add the partial sums in the dot product operation, though
% those partial sums must be computed at the same time. Increasing the
% number of multiply operations, however, increases the output range of
% an APE. Since an analog computation unit produces more accurate
% results when it is optimized for a small range of inputs, accuracy is
% compromised for efficiency.  One challenge is to determine a
% configuration of APEs that capitalizes on opportunities for efficiency
% while maintaining high accuracy for the largest number of applications. 


\paragraph{Bit-width.}
One design choice is the optimal bit-width of inputs and weights sent from
the CPU to the ANPU. Unlike a digital design that would see increased
(or at least constant) accuracy with an increased number of bits, analog
nonidealities associated with an increased number of bits, as well
as increased signal ranges due to a larger number of possible values,
can result in decreased accuracy. Therefore, although it may seem that
supporting larger bit-width would result in higher accuracy and, subsequently,
increased application coverage, that is not necessarily the case in an analog design. Again,
flexibility and potential accuracy must be weighed against actual circuit-level computation accuracy. 

Each of the design components described has its own tradeoffs in power, speed, and accuracy; however,
the components are related to each other, and in
order to optimize overall accuracy and flexibility,
changes in one will affect design choices within another.
We suggest a strategy of fixing one component, while exploring
possibilities within the other two. We have designed an APE circuit
that utilizes particular value representations, and we have considered a simple 
APE configuration from which to explore bit-width implications on overall accuracy and application coverage. 
Next we describe our initial design, highlighting related challenges and tradeoffs. 
Section~\ref{sec:eval} describes our preliminary results in 
bit-width evaluation, and 
Section~\ref{sec:future} describes our future
work, which includes a more thorough study in designing
an ideal APE configuration.


% \paragraph{Hardware/Software Accuracy:}
% In a mixed-signal computing approach, a tradeoff exists between
% achievable hardware and software accuracy. For example, analog
% circuits are designed to function well within defined ranges, limited
% by the operating ranges of the transistors. This requirement can place
% restrictions on the compiler's code-transformation phase in selecting the optimal
% neural network topology, limiting achievable accuracy.  One challenge
% is to identify the point of optimal accuracy, considering the impact
% of various design choices on both hardware and software accuracy
% (potential accuracy).


%\paragraph{Efficiency/Accuracy:}
%An analog approach introduces a fine-grained accuracy/efficiency
%tradeoff not present in a digital design. 
%Digital techniques for approximate computing~\cite{} can also trade accuracy for energy
%efficiently, but at a significantly larger granularity. For example,
%techniques that utilize supply voltage scaling would require
%fine-grained control for adjusting of the supply voltage.  



% For example, to compute the dot product, 
% the digital PE time multiplexes the multiply-accumulate
% operations. 
% In an analog design, the multiply operations are computed simulatensouly for
% easy summation, for example, through KCL. This places limits on the
% number of APE inputs in the analog approach, since analog circuits
% are optimized to function well within certain ranges, limited by the
% operating ranges of the transistors. To maximize accuracy at the
% circuit level, the number of APE inputs would be kept at a minimum.

% Optimizing accuracy at the code-transformation level, however, 
% would require a flexible number of inputs
. 

%To gain computation efficiency in 
%the analog domain, flexibility may be limited at the software level,
%for example limiting the number of network 
%This, however, limits on the
%The challenge is to find the best point considering both. 

%At the application level, flexibility in network topology allows for more
%accurate outputs. 

%At the circuit level, however, flexibility reduces
%energy savings and accuracy.  
%Overall, the CPU-ANPU combination aims
%to maximize accuracy, while minimizing energy consumption.  
