\section{An Analog Processing Engine Design}
\label{sec:design}


\begin{figure}
\centering
    \subfloat[APE top-level diagram]{
        \includegraphics[width=.45\textwidth]{./figs/Anpu/APE.pdf}
        \label{fig:ape_top}}\\
    \subfloat[Differential pair APE component]{
        \includegraphics[width=0.12\textwidth]{./figs/Anpu/diffPair.pdf}
        \label{fig:diffpair}}
    \caption{A single analog processing engine}
    \label{fig:ape}
\end{figure}


%This section outlines the design of an analog processing engine (APE)
%and a configuration of APEs that constitutes an ANPU. 

% For the initial design described here, we map digital input values to
% the analog space using current-steering DACs that translate digital
% values to analog currents. A digital input value is represented on two
% wires, as two currents - one corresponding to a positive value,
% and one corresponding to a negative value. 
% We map weight values to the analog domain using
% a resistor ladder network. 
% To start, we set
% the analog/digital boundary at the
% level of APE inputs and outputs. 
% Multiple APEs are physically configured into a
% single row.
%which is able to compute one layer of the neural network per clock
%cycle for small networks

%which is comparable to the digital PE described in the previous
%section.
%to compute a sigmoid-activated 
 %followed by the
%design of an ANPU, which is comparable to the digital NPU.



%\subsection{APE Design} 

Figure~\ref{fig:ape} shows a single APE.  To compute a
sigmoid-activated neuron, a sigmoid function is applied to the dot
product of a set of inputs and their corresponding weights. Inputs and
weights are represented in sign-magnitude form.  
Digital input values map to
the analog space through current-steering DACs that translate digital
values to analog currents.
A digital input value is represented on two
wires, as two currents - one corresponding to a positive value,
and one corresponding to a negative value. 
In the Figure~\ref{fig:ape_top}, $I_{input+}$ is the
current that represents a positive input value, where $I_{input-}$
represents a negative one.  Standard resistor string ladders function
to scale the input current by the digital weight value, effectively
multiplying each input by its corresponding weight.  The resistor
ladder consists of a set of resistors connected to a tree-structured
set of switches. The digital weight bits control the switches,
adjusting the effective resistance, $R_{effective}$, seen by the input
current. The output of the resistor ladder is a voltage: $V_{scaled} =
I_{input} * R_{effective}$.  The resistor network requires $2^N$
resistors and approximately $2^{N+1}$ switches, where $N$ is the
number of digital weight bits.  This resistor ladder design has been
shown to work well for $N\le10$. %TODO: \cite{}
Our circuit simulations show that only minimally sized switches are
required here.

$V-_{scaled}$ can be interpreted as
$V-_{scaled} = V_{cm} - V$, and similarly, $V+_{scaled} = V_{cm} + V$, where $V_{cm}$ is a common
mode voltage, and $V$ is proportional to the unsigned product of the input
and the weight.  
$V-_{scaled}$ and $V+_{scaled}$,
control the gate voltages of a differential pair shown in
Figure~\ref{fig:diffpair}.  
The current through each transistor in the differential
pair is proportional to the voltage applied at its gate.  If the
voltage difference between the two gates is kept small, the
current-voltage relationship is linear, producing $I_{out+} =
\frac{I_{bias}}{2} + \Delta I$ and $I_{out-} =\frac{I_{bias}}{2} - \Delta I$. Resistor
ladder values are chosen such that the gate voltages are in a range
that produce linear outputs, and consequently a more accurate final
result.  Based on the sign of the computation, a switch steers either
the current associated with a positive value or the current associated
with a negative value to a single wire to be efficiently summed according to
Kirchhoff's current law.

A resistor converts the final current to a voltage, and an
analog-to-digital converter converts the voltage to a digital value.
We chose a flash ADC design (named for its speed), which consists of a
set of reference voltages and comparators~\cite{Allen:AnalogDesign, Johns:AnalogDesign}.  The ADC requires $2^N$
comparators, where $N$ is the number of digital output bits.
Typically, ADC reference voltages increase linearly; however, we use a
non-linearly increasing set of reference voltages to capture the
behavior of a sigmoid function. As such, this design allows for a very
accurate sigmoid operation.

\paragraph{Considerations.} 
In regards to bit-width, the selected mapping of inputs to the analog
domain results in an exponential increase in static
power consumption with increased bit-width. One should
consider whether an increased number of input bits is worth
the increase in power consumption.  
Also, increasing the number of DAC input bits can
negatively affect accuracy as the resulting current value
scales less ideally with increases in bit-width. 
The number of weight bits plays a less
obvious role in power consumption. The effect of an increase in the
number of weight bits mostly materializes as increased delay.

In contrast to the digital PE design, the APE design has a fixed
number of input-weight combinations that can contribute to the
dot-product operation.  This is required since all multiply
operations occur simultaneously so that they may be easily summed. As
a result, some hardware will go unused when fewer multiply operations
are needed to compute the output of a neuron. Alternately, at the
code-transformation level, the number of
inputs to a neuron must be restricted, limiting flexibility, and
perhaps application coverage.  
Increasing the computation width of an APE can have a negative effect on
circuit accuracy because the circuit is optimized to work within a
certain range, and increasing the width will widen the range of
possible voltage and current signals; however, imposing limits on the neural
network topology specified by the compiler can also negatively impact
the potential achievable accuracy.
For example, we've found that the ideal operating range at the gate inputs of the
differential pairs, which are used to add the partial sums, is only about 60
mV. This small range limits both the number of computations that can be performed
in each APE, as well as the bit-width of the inputs and weights.

The initial APE design involves data conversions at the front
and back end. The ADC significantly contributes to dynamic power
consumption with its large number of switching comparators.  
Additionally, this switching causes decreased accuracy by affecting
near-by signals. Lastly, the ADC acts to quantize the output value,
limiting precision based on the number of ADC output bits.  

\if 0

For our initial APE design, we have chosen an 8-wide APE to maximize
power, speed, and circuit accuracy for the general case (8 is wide enough for half
of the benchmarks studied).  To support networks
of any size, we present
a strategy to deal with {\em larger networks} -- networks
whose neurons require more inputs than can be supported by a single
APE.


\subsection{ANPU Design}

\begin{figure*}
  \centering
  \includegraphics[width=0.7\textwidth]{./figs/Anpu/ANPUsmallntwk.pdf}
  \caption{An analog neural processing unit}
  \label{fig:ANPUsmallnetwork}
\end{figure*}

%TODO: We may want to get rid of most of this section (describing the
%switch network solution), and instead discuss analog storage (or signal buffering) here
%and say that it is future work. 

Similar to the digital NPU design, the ANPU must receive input and weight values from the CPU,
do all computation for a given neural topology, and send the outputs back to the CPU.
Figure~\ref{fig:ANPUsmallnetwork} shows an ANPU that uses the APE design described above. 
For small networks, each APE computes one neuron in the topology, with 
the assumption that the number of inputs to the neuron does not exceed
the computation width
%(the number of input/weight pairs that can be multiplied in a given cycle) 
of the APE. Our ANPU design configures 8 APEs into a single row. 
We choose the number of APEs equal to the APE computation width to easily support fully-connected networks. 
The CPU sends inputs and weights and retrieves output values using enqueue and dequeue instructions
(ISA extensions), similar to the manner described in Section~\ref{sec:npu} for the digital NPU. The control FIFO holds the necessary control bits
for each cycle, such as a source select and write enable bits. 
To start computation for the neural network, each APE computes using a set of inputs sent from the input FIFO.
The 8 digital outputs of the APE array constitute the inputs for all
APEs on the next cycle. This scheme allows
for any number of neural network layers. The results of the final iteration are placed in the output queue.

A second reason for choosing an 8 APE array configuration is that this configuration, with APEs capable
of 8-wide computation, allows for 64 multiplies in a single cycle. The largest networks selected for
the benchmarks studied consist of at most 64 inputs per neuron, since
the added benefit of increasing network size beyond this point are limited. To deal with larger networks, we
propose the addition of a switch network that allows the current outputs of multiple APEs to be combined, 
through Kirchhoff's current law, before ADC conversion. For example, to compute a neuron with 64 inputs, 
the ANPU utilizes the multiply-add functionality of all 8 APEs, but the output current of each APE 
is steered to a single ADC, rather than each current being directed toward its respective ADC. Similarly,
when 16 inputs are required, the 8 APEs can be logically grouped in pairs to compute 4 neurons per cycle. 
For an ANPU with 8-PEs, 28 switches are required to realize the full range of possible combinations. 
This design choice to increase network flexibility adds complexity in writing to the APE input FIFOs.
Switch control bits, as well as the additional write enable bits, are determined statically and sent to
the NPU via the configuration FIFO.


\paragraph{Considerations and Limitations:}

Just as increasing the computation width of an APE may decrease accuracy and cause 
functional issues due to increased range,
combining the outputs of multiple APEs may result in similar effects. Future work will determine
where the limits are. 

\fi

%What are the mechanisms, challenges, and requirements?
%variation
%accretion of noise
%control
%conversion overheads (storage)

%Also, we envision a future ANPU design with multiple rows of APEs,
%removing data conversions to better capitalize on the energy
%advantages of an analog design, as well as removing imprecision due to
%quantization at each APE output. Chapter~\ref{ch:plan} describes plans
%for an improved ANPU design.
